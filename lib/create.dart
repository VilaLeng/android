import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Create extends StatefulWidget {
  @override
  _CreateState createState() => _CreateState();
}

class _CreateState extends State<Create> {
  // Handles text
  TextEditingController nameController = TextEditingController();
  TextEditingController priceController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();

  // Http post request to create new data
  Future _createProducts() async {
    return await http.post(
      Uri.parse("http://192.168.2.122/android/insert_product.php"),
      body: {
        "name": nameController.text,
        "price": priceController.text,
        "description": descriptionController.text,
      },
    );
  }

  void _onConfirm(context) async {
    await _createProducts();

    // Remove all existing routes until the Home.dart, then rebuild Home.
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add Products"),
      ),
      bottomNavigationBar: BottomAppBar(
        child: ElevatedButton.icon(
          label: Text('Save'),
          icon: Icon(Icons.save),
          style: ElevatedButton.styleFrom(
            primary: Colors.blue,
            onPrimary: Colors.white,
          ),
          onPressed: () {
            _onConfirm(context);
          },
        ),
      ),
      body: Container(
        height: double.infinity,
        padding: EdgeInsets.all(20),
        child: Column(
          children: <Widget>[
            Container(
                child: TextField(
              controller: nameController,
              decoration: InputDecoration(
                labelText: "Product Name:",
                hintText: "Enter Product Name",
              ),
            )),
            Container(
                child: TextField(
              controller: priceController,
              decoration: InputDecoration(
                labelText: "Price:",
                hintText: "Enter Product Price",
              ),
            )),
            Container(
                child: TextField(
                  controller: descriptionController,
                  decoration: InputDecoration(
                    labelText: "Description:",
                    hintText: "Enter Product Description",
                  ),
                )),
          ],
        ),
      ),
    );
  }
}
